<?php
/*
Plugin Name: Woo Size Chart
Plugin URI: https://cuongwp.com/
Description: Adds a size chart feature to WooCommerce.
Version: 1.0.2
Author: CuongPham
Author URI: https://cuongwp.com/
*/

define('WOO_SIZE_CHART_PLUGIN_DIR', '/wp-content/plugins/woo-size-chart/');
// Enqueue necessary scripts and stylesheets
function cuongwp_size_chart_enqueue_scripts() {
    wp_enqueue_style('fancybox-css', plugins_url('css/fancybox.css', __FILE__));
    wp_enqueue_style('woo-size-chart-css', plugins_url('css/woo-size-chart.css', __FILE__));
    wp_enqueue_script('fancybox-js', plugins_url('js/fancybox.js', __FILE__), array('jquery'), '1.0', true);
    wp_enqueue_script('woo-size-chart-js', plugins_url('js/woo-size-chart.js', __FILE__), array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'cuongwp_size_chart_enqueue_scripts');

// Register the custom post type for size charts
function cuongwp_register_size_chart_post_type() {
    $labels = array(
        'name' => __('Size Charts', 'cuongwp-woo-size-chart'),
        'singular_name' => __('Size Chart', 'cuongwp-woo-size-chart'),
        'add_new' => __('Add New', 'cuongwp-woo-size-chart'),
        'add_new_item' => __('Add New Size Chart', 'cuongwp-woo-size-chart'),
        'edit_item' => __('Edit Size Chart', 'cuongwp-woo-size-chart'),
        'new_item' => __('New Size Chart', 'cuongwp-woo-size-chart'),
        'view_item' => __('View Size Chart', 'cuongwp-woo-size-chart'),
        'search_items' => __('Search Size Charts', 'cuongwp-woo-size-chart'),
        'not_found' => __('No size charts found', 'cuongwp-woo-size-chart'),
        'not_found_in_trash' => __('No size charts found in Trash', 'cuongwp-woo-size-chart'),
        'parent_item_colon' => __('Parent Size Chart:', 'cuongwp-woo-size-chart'),
        'menu_name' => __('Woo Size Charts', 'cuongwp-woo-size-chart'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'public' => false, // Set to true if you want it publicly accessible
        'publicly_queryable' => false, // Set to true if you want it publicly queryable
        'show_ui' => true,
        'menu_icon' => plugins_url('img/ruler-measure-white.svg', __FILE__),
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'supports' => array('title', 'editor'),
        'taxonomies' => array('product_cat'), // Add this line to enable the product category taxonomy
    );

    register_post_type('cuongwp_size_chart', $args);
}
add_action('init', 'cuongwp_register_size_chart_post_type');

// Display the "View Size Chart" button on product pages
function cuongwp_display_size_chart_button() {
    global $post;

    if (is_singular('product')) {
        $categories = get_the_terms($post->ID, 'product_cat');

        if ($categories) {
            $category_slugs = wp_list_pluck($categories, 'slug');
            $size_charts = get_posts(array(
                'post_type' => 'cuongwp_size_chart',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => $category_slugs,
                    ),
                ),
            ));

            if ($size_charts) {
                $button_template = plugin_dir_path(__FILE__) . 'templates/content-button.php';
                include $button_template;
            }
        }
    }
}
add_action('woocommerce_single_product_summary', 'cuongwp_display_size_chart_button', 25);

// Add size chart content to the popup
function cuongwp_display_size_chart_popup() {
    if (is_singular('product')) {
        global $post;

        $categories = get_the_terms($post->ID, 'product_cat');

        if ($categories) {
            $category_slugs = wp_list_pluck($categories, 'slug');
            $size_charts = get_posts(array(
                'post_type' => 'cuongwp_size_chart',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => $category_slugs,
                    ),
                ),
            ));

            if ($size_charts) {
                $template = plugin_dir_path(__FILE__) . 'templates/content-sizechart.php';
                include $template;
            }
        }
    }
}
add_action('wp_footer', 'cuongwp_display_size_chart_popup');

// Add credit text below editor
function cuongwp_add_credit_text() {
    global $post;

    if ($post->post_type === 'cuongwp_size_chart') {
        ?>
            <p><b>Plugin Woo Size Chart được chia sẻ MIỄN PHÍ, cấm các hình thức mua bán</b></p>
            <p>Bạn có thể ủng hộ mình một ly cafe bằng cách chuyển vào MOMO số điện thoại <a href="https://me.momo.vn/cuongwp"
                    target="_blank">0794652822</a>.</p>
            <p>Nếu bạn cần hỗ trợ hoặc thiết kế website thì add Zalo mình nhé :)</p>
        <?php
    }
}
add_action('edit_form_after_editor', 'cuongwp_add_credit_text');