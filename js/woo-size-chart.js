(function ($) {
    $('#size-chart-popup-button').on('click', function(e){
        e.preventDefault();
        Fancybox.show([{ src: "#size-chart-popup-content", type: "inline" }]);
    })
})(jQuery);
