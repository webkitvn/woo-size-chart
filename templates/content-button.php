<div class="size-chart-button-container">
    <button id="size-chart-popup-button" data-fancybox data-src="size-chart-popup-content" class="size-chart-button fancybox">
        <img src="<?php echo WOO_SIZE_CHART_PLUGIN_DIR ?>img/ruler-measure.svg" alt="ruler" width="24" height="24">
        <span><?php _e('Size Guide', 'cuongwp-woo-size-chart'); ?></span>
    </button>
</div>