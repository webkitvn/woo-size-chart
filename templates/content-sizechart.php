<div id="size-chart-popup-content" data-fancybox="size-chart-popup-content" class="size-chart-popup"
    style="display:none; width: 1000px;max-width: 98%">
    <div class="size-chart-content">
        <?php if (!empty($size_charts)) : ?>
            <?php
                $first_size_chart = reset($size_charts); // Get the first size chart
                echo apply_filters( 'the_content', $first_size_chart->post_content );
            ?>
        <?php endif; ?>
    </div>
</div>